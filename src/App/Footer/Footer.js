import React from 'react';

import './footer.css'
import Logo from './Logo/Logo'
import Copyright from './Copyright/Copyright'
import Contact from './Contact/Contact'

const Footer = () => {
    return (
      <footer className="footer">		
          <div className="container">
              <div className="row">
                    <div className="col-lg-3">
                        <Logo/>
                    </div>
                    <div className="col-lg-6">
                        <Copyright/>
                    </div>
                    <div className="col-lg-3">
                        <Contact/>
                    </div>
                </div>
             </div>
      </footer>
    )
  }

export default Footer