import React from 'react'
import "./BrNewsListItem.css"


const BrNewsListItem = ({
    name,
    newsdate,
    type,
    image,
}) => {
        return (
            <div className='br-news-list-item'>
                <div className='br-news-img col-'>
                <img src={image} alt={name} className='br-news-img'/>
                </div>
                <div className="br-news-features"><a href="#">{type}</a></div>
                <div className="br-news-date">{newsdate}</div>
                <h2  className='br-news-title'>{name}</h2>   
            </div>

        )}

export default BrNewsListItem