import React from 'react'
import './breakingnews.css'
import BrNewsListItem from './BrNewsListItem'
import brnews from './brnews' 

const BreakingNews = () => {
return (
    <div>
          <h1 className='title-page'>Breaking News</h1>
             <div className='row'>
                {
                    brnews.map((
                        {
                            id,
                            name,
                            type,
                            newsdate,
                            image,                        
                        }

                    ) => {
                        return (
                            <div className='col-lg-10' key={id}>
                                <BrNewsListItem
                                    image={image}
                                    type={type}
                                    name={name}
                                    newsdate={newsdate}
                                />
                            </div>
                            
                        )
                    })  
                }
          </div>
        </div>
)
}
export default BreakingNews