const brnews = [
    {
        id:1,
        name:'Latest Technology News',
        newsdate:'7:20 AM | March 14',
        type:'TECHNOLOGY',
        image:'/images/news/news-sm1.jpg'
    },
    {
        id:2,
        name:'World Cup 2019',
        newsdate:'7:18 AM | March 14',
        type:'SPORT',
        image:'/images/news/news-sm3.jpg'
    },
    {
        id:3,
        name:'Financial Crisis In The World',
        newsdate:'7:15 AM | March 14',
        type:'FINANCE',
        image:'/images/news/news-sm4.jpg'
    },
    {
        id:4,
        name:'Presidential Election News',
        newsdate:'7:00 AM | March 14',
        type:'POLITICS',
        image:'/images/news/news-sm2.jpg'
    },
    {
        id:5,
        name:'Latest Technology News',
        newsdate:'7:20 AM | March 14',
        type:'TECHNOLOGY',
        image:'/images/news/news-sm1.jpg'
    },
    {
        id:6,
        name:'World Cup 2019',
        newsdate:'7:18 AM | March 14',
        type:'SPORT',
        image:'/images/news/news-sm3.jpg'
    },
    {
        id:7,
        name:'Financial Crisis In The World',
        newsdate:'7:15 AM | March 14',
        type:'FINANCE',
        image:'/images/news/news-sm4.jpg'
    },
    {
        id:8,
        name:'Presidential Election News',
        newsdate:'7:00 AM | March 14',
        type:'POLITICS',
        image:'/images/news/news-sm2.jpg'
    },
    {
        id:9,
        name:'Latest Technology News',
        newsdate:'7:20 AM | March 14',
        type:'TECHNOLOGY',
        image:'/images/news/news-sm1.jpg'
    },
    {
        id:10,
        name:'World Cup 2019',
        newsdate:'7:18 AM | March 14',
        type:'SPORT',
        image:'/images/news/news-sm3.jpg'
    },
    {
        id:11,
        name:'Financial Crisis In The World',
        newsdate:'7:15 AM | March 14',
        type:'FINANCE',
        image:'/images/news/news-sm4.jpg'
    },
    {
        id:12,
        name:'Presidential Election News',
        newsdate:'7:00 AM | March 14',
        type:'POLITICS',
        image:'/images/news/news-sm2.jpg'
    },
    {
        id:13,
        name:'World Cup 2019',
        newsdate:'7:18 AM | March 14',
        type:'SPORT',
        image:'/images/news/news-sm3.jpg'
    },
    {
        id:14,
        name:'Financial Crisis In The World',
        newsdate:'7:15 AM | March 14',
        type:'FINANCE',
        image:'/images/news/news-sm4.jpg'
    },
    {
        id:15,
        name:'Presidential Election News',
        newsdate:'7:00 AM | March 14',
        type:'POLITICS',
        image:'/images/news/news-sm2.jpg'
    },

]
export default brnews