import React from 'react';
import NewsList from './News/NewsList';
import BreakingNews from './BreakingNews/BreakingNews'

const Main = () => {
    return (
        <main className="main">
          <div className="container">
              <div className="row">
                  <div className="col-lg-9">
                      <NewsList/>
                  </div>
                  <div className="col-lg-3">
                      <BreakingNews/>
                  </div>
              </div>
          </div>
      </main>
    )
  }
  
  export default Main