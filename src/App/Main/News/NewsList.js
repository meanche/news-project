import React from 'react'
import './NewsList.css'
import NewsListItem from './NewsListItem'
import news from './news' 
import NewsMainItem from './NewsMainItem'
import mainnews from './mainnews'


const NewsList = () => {
    return (
        <div>
          {/* <h1 className='title-page'>Last News</h1> */}
             <div className='row'>
{
    mainnews.map((
        {
            id,
            name,
            description,
            type,
            image,                        
        }

    ) => {
        return (
            <div>
                
                <div className='col-lg-60' key={id}>
                    <NewsMainItem
                        image={image}
                        name={name}
                        description={description}
                        type={type}
                    />
                </div>

            </div>
            
            
        )
    })  
}

                {
                    news.map((
                        {
                            id,
                            name,
                            description,
                            type,
                            image,                        
                        }

                    ) => {
                        return (
                            <div>
                                
                                <div className='col-lg-40' key={id}>
                                    <NewsListItem
                                        image={image}
                                        name={name}
                                        description={description}
                                        type={type}
                                    />
                                </div>

                            </div>
                            
                            
                        )
                    })  
                }
          </div>
        </div>
    )
}
export default NewsList