import React from 'react'
import "./NewsListItem.css"


const NewsListItem = ({
    name,
    description,
    type,
    image,
}) => {
        return (
            <div className='news-list-item'>
                <div className='news-img'>
                <img src={image} alt={name} className='news-img'/>
                </div>
                <h2  className='news-title'>{name}</h2>
                <div className="news-features"><a href="#">{type}</a></div>
                <div className="news-description">{description}</div>
            </div>

        )}

export default NewsListItem