import React from 'react'
import "./NewsMainItem.css"


const NewsMainItem = ({
    name,
    description,
    type,
    image,
}) => {
        return (
            <div className='mainnews-list-item'>
                <div className='mainnews-img'>
                <img src={image} alt={name} className='mainnews-img'/>
                </div>
                <h2  className='mainnews-title'>{name}</h2>
                <div className="mainnews-features"><a href="#">{type}</a></div>
                <div className="mainnews-description">{description}</div>
            </div>

        )}

export default NewsMainItem