const mainnews = [
    {
        id:1,
        name:'Parliament debates seizing control of Brexit',
        description:'One of the stranger elements of today is how much of a broken record the House of Commons sounds. The UK political class has, throughout the entire Brexit process, become very good at debating exclusively with itself, seemingly unaware that people in Brussels – and across Europe – are paying attention to their Brexit delusions. Some have held strong to the idea that at the last minute the EU would blink and offer the UK the deal of a lifetime. Others have believed that the EU would take control of the process effectively willing Brussels to interfere in the UKs domestic politics. Last week the EU gave absolute clarity on its position: it wants to avoid a no deal, but the UKs destiny really is in the UKs hands. Now is the time for the House of Commons to come up with something. Given the sense of urgency, its nothing short of extraordinary that, once again, we have listened to MPs talk for hours about the same old stuff.',
        type:'ECONOMIC',
        image:'/images/news/mainnews.jpg'
    },
    
]
export default mainnews