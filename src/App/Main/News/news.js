const news = [
    {
        id:1,
        name:'Latest Technology News',
        description:'The latest tech news about the worlds best (and sometimes worst) hardware, apps, and much more. From top companies like Google...',
        type:'TECHNOLOGY',
        image:'/images/news/news1.png'
    },
    {
        id:2,
        name:'World Cup 2019',
        description:'Plot your teams progress to the final - heres the full fixtures for the ICC Mens Cricket World Cup 2019, to be held in England and Wales.',
        type:'SPORT',
        image:'/images/news/news3.png'
    },
    {
        id:3,
        name:'Financial Crisis In The World',
        description:'A financial crisis is any of a broad variety of situations in which some financial assets suddenly. Other global and national financial...',
        type:'FINANCE',
        image:'/images/news/news4.png'
    },

]
export default news